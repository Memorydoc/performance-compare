# performance-compare

#### 介绍
并发编程框架性能对比、测试用例等 配合动态线程池进行性能压测对比

## 对比项目
* gobrs-async
* liteflow
* asyncTool

## 辅助工具
* HIPPO-4J 动态线程池监控工具: https://hippo4j.cn/ (使用方式请参考官网) 
* jmeter



