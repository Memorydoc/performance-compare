package com.example.performancecompare.liteflow;

import com.yomahub.liteflow.core.NodeComponent;
import org.springframework.stereotype.Component;

/**
 * @program: performance-compare
 * @ClassName CCmp
 * @description:
 * @author: sizegang
 * @create: 2022-12-09
 **/
@Component("c")
public class CCmp extends NodeComponent {

    @Override
    public void process() throws InterruptedException {
        System.out.println("使用" + Thread.currentThread().getName());
        System.out.println("c");
        Thread.sleep(100);
    }
}
