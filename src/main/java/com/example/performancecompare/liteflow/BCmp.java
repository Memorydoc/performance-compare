package com.example.performancecompare.liteflow;

import com.yomahub.liteflow.core.NodeComponent;
import org.springframework.stereotype.Component;

/**
 * @program: performance-compare
 * @ClassName Bcmp
 * @description:
 * @author: sizegang
 * @create: 2022-12-09
 **/
@Component("b")
public class BCmp extends NodeComponent {

    @Override
    public void process() throws InterruptedException {
        System.out.println("使用" + Thread.currentThread().getName());
        System.out.println("b");
        Thread.sleep(100);
    }
}