package com.example.performancecompare.liteflow;

import com.yomahub.liteflow.core.NodeComponent;
import org.springframework.stereotype.Component;

/**
 * @program: performance-compare
 * @ClassName ACmp
 * @description:
 * @author: sizegang
 * @create: 2022-12-09
 **/
@Component("a")
public class ACmp extends NodeComponent {

    @Override
    public void process() throws InterruptedException {
        System.out.println("使用" + Thread.currentThread().getName());
        System.out.println("a");
        Thread.sleep(100);
    }
}