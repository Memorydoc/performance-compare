package com.example.performancecompare.gobrs;

import com.gobrs.async.core.TaskSupport;
import com.gobrs.async.core.anno.Task;
import com.gobrs.async.core.task.AsyncTask;
import lombok.SneakyThrows;

/**
 * @program: performance-compare
 * @ClassName TaskB
 * @description:
 * @author: sizegang
 * @create: 2022-12-09
 **/
@Task(failSubExec = true,timeoutInMilliseconds = 100)
public class TaskB extends AsyncTask {

    int l = 0;

    @SneakyThrows
    @Override
    public Object task(Object o, TaskSupport support) {
        System.out.println("使用" + Thread.currentThread().getName());
        System.out.println("TaskB Begin");
        Thread.sleep(100);
//        for (int i = 0; i < 11111111111111L; i++) {
//            l++;
//        }
        System.out.println("TaskB End");
        return null;
    }
}
