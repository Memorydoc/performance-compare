package com.example.performancecompare.completable;

/**
 * @program: performance-compare
 * @ClassName TaskA
 * @description:
 * @author: sizegang
 * @create: 2022-12-16
 **/
public class TaskC implements Runnable{

    @Override
    public void run() {
        System.out.println("taskC begin");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("taskC end");
    }
}
