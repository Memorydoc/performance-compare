package com.example.performancecompare;

import cn.hippo4j.core.enable.EnableDynamicThreadPool;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDynamicThreadPool
@SpringBootApplication
public class PerformanceCompareApplication {

    public static void main(String[] args) {
        SpringApplication.run(PerformanceCompareApplication.class, args);
    }

}
