package com.example.performancecompare.controller;

import com.example.performancecompare.asynctool.*;
import com.example.performancecompare.completable.*;
import com.gobrs.async.core.GobrsAsync;
import com.jd.platform.async.executor.Async;
import com.jd.platform.async.wrapper.WorkerWrapper;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.*;

/**
 * The type Test controller.
 *
 * @program: performance -compare
 * @ClassName TestController
 * @description:
 * @author: sizegang
 * @create: 2022 -12-09
 */
@RestController
@RequestMapping("test")
public class TestController {

    @Resource
    private FlowExecutor flowExecutor;

    @Resource
    private GobrsAsync gobrsAsync;

    @Resource
    private ThreadPoolExecutor asyncToolThreadPool;


    private ExecutorService executorService = Executors.newCachedThreadPool();


    /**
     * Completable string.
     *
     * @return the string
     */
    @RequestMapping("completable")
    public String completable() {
        long start = System.currentTimeMillis();
        try {
            CompletableFuture
                    .runAsync(new TaskA(), executorService).thenRun(() -> CompletableFuture.allOf(CompletableFuture.runAsync(new TaskB(), executorService), CompletableFuture.runAsync(new TaskC(), executorService)).join())
                    .thenRun(new TaskD())
                    .thenRun(() -> CompletableFuture.allOf(CompletableFuture.runAsync(new TaskE(), executorService), CompletableFuture.runAsync(new TaskF(), executorService)).join()).join();
            return "success";
        } catch (Exception e) {
            return "fail";
        } finally {
            System.out.println("消耗 " + (System.currentTimeMillis() - start));
        }
    }


    /**
     * Testgobrs string.
     *
     * @return the string
     */
    @RequestMapping("gobrs")
    public String testgobrs() {
        System.out.println("主线程使用" + Thread.currentThread().getName());
        long start = System.currentTimeMillis();
        gobrsAsync.go("chain1", () -> "args");
        System.out.println("耗时" + (System.currentTimeMillis() - start));
        return Strings.EMPTY;
    }

    /**
     * Test lite flow string.
     *
     * @return the string
     */
    @RequestMapping("liteflow")
    public String testLiteFlow() {
        System.out.println("主线程使用" + Thread.currentThread().getName());
        long start = System.currentTimeMillis();
        LiteflowResponse response = flowExecutor.execute2Resp("chain1", "arg");
        System.out.println("耗时" + (System.currentTimeMillis() - start));
        return Strings.EMPTY;
    }


    /**
     * Test asynctool string.
     *
     * @return the string
     */
    @RequestMapping("asynctool")
    public String testAsynctool() {
        System.out.println("主线程使用" + Thread.currentThread().getName());
        long start = System.currentTimeMillis();

        AsyncToolTaskA a = new AsyncToolTaskA();
        AsyncToolTaskB b = new AsyncToolTaskB();
        AsyncToolTaskC c = new AsyncToolTaskC();
        AsyncToolTaskD d = new AsyncToolTaskD();
        AsyncToolTaskE e = new AsyncToolTaskE();
        AsyncToolTaskF f = new AsyncToolTaskF();


        WorkerWrapper<String, String> fWorker = new WorkerWrapper.Builder<String, String>()
                .worker(f)
                .build();

        WorkerWrapper<String, String> eWorker = new WorkerWrapper.Builder<String, String>()
                .worker(e)
                .build();

        WorkerWrapper<String, String> dWorker = new WorkerWrapper.Builder<String, String>()
                .worker(d)
                .next(eWorker, fWorker)
                .build();

        WorkerWrapper<String, String> cWorker = new WorkerWrapper.Builder<String, String>()
                .worker(c)
                .next(dWorker)
                .build();

        WorkerWrapper<String, String> bWorker = new WorkerWrapper.Builder<String, String>()
                .worker(b)
                .next(dWorker)
                .build();

        WorkerWrapper<String, String> aWork = new WorkerWrapper.Builder<String, String>()
                .worker(a)
                .next(bWorker, cWorker)
                .build();

        try {
            Async.beginWork(10000, aWork);
            Async.beginWork(10000, executorService, aWork);
        } catch (ExecutionException ex) {
            ex.printStackTrace();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        System.out.println("耗时" + (System.currentTimeMillis() - start));
        return Strings.EMPTY;
    }

}
