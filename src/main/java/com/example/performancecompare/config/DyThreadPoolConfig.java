package com.example.performancecompare.config;

/**
 * @program: performance-compare
 * @ClassName ThreadPoolConfig
 * @description:
 * @author: sizegang
 * @create: 2022-12-10
 **/

import cn.hippo4j.core.executor.DynamicThreadPool;
import cn.hippo4j.core.executor.support.ThreadPoolBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ThreadPoolExecutor;

@Configuration
public class DyThreadPoolConfig {

    @Bean
    @DynamicThreadPool
    public ThreadPoolExecutor gobrsThreadPool() {
        String threadPoolId = "gobrsThreadPool";
        ThreadPoolExecutor messageConsumeDynamicExecutor = ThreadPoolBuilder.builder()
                .threadFactory(threadPoolId)
                .threadPoolId(threadPoolId)
                .dynamicPool()
                .build();
        return messageConsumeDynamicExecutor;
    }

    @Bean
    @DynamicThreadPool
    public ThreadPoolExecutor liteflowThreadPool() {
        String threadPoolId = "liteflowThreadPool";
        ThreadPoolExecutor messageProduceDynamicExecutor = ThreadPoolBuilder.builder()
                .threadFactory(threadPoolId)
                .threadPoolId(threadPoolId)
                .dynamicPool()
                .build();
        return messageProduceDynamicExecutor;
    }

    @Bean
    @DynamicThreadPool
    public ThreadPoolExecutor asyncToolThreadPool() {
        String threadPoolId = "asyncToolThreadPool";
        ThreadPoolExecutor messageProduceDynamicExecutor = ThreadPoolBuilder.builder()
                .threadFactory(threadPoolId)
                .threadPoolId(threadPoolId)
                .dynamicPool()
                .build();
        return messageProduceDynamicExecutor;
    }

}