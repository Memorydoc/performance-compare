package com.example.performancecompare.config;

import com.gobrs.async.core.threadpool.GobrsAsyncThreadPoolFactory;
import com.gobrs.async.core.threadpool.GobrsThreadPoolConfiguration;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * The type Thread pool com.gobrs.async.config.
 *
 * @program: gobrs -async
 * @ClassName ThreadPoolConfig
 * @description: Custom thread pool configuration
 * @author: sizegang
 * @create: 2022 -02-20 00:34
 * @Version 1.0
 */
@Configuration
public class GobrsThreadPoolConfig extends GobrsThreadPoolConfiguration {

    @Resource
    private ThreadPoolExecutor gobrsThreadPool;

    @Override
    protected void doInitialize(GobrsAsyncThreadPoolFactory factory) {
        /**
         * 自定义线程池
         */
//        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(300, 500, 30, TimeUnit.SECONDS,
//                new LinkedBlockingQueue());

//        ExecutorService executorService = Executors.newCachedThreadPool();

        factory.setThreadPoolExecutor(Executors.newCachedThreadPool());
    }
}
