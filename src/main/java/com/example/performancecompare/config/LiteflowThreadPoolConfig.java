package com.example.performancecompare.config;

import com.yomahub.liteflow.thread.ExecutorBuilder;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @program: performance-compare
 * @ClassName LiteflowThreadPoolConfig
 * @description:
 * @author: sizegang
 * @create: 2022-12-10
 **/
@Configuration
public class LiteflowThreadPoolConfig implements ExecutorBuilder {


    @Resource
    private ThreadPoolExecutor liteflowThreadPool;

    @Override
    public ExecutorService buildExecutor() {
        return Executors.newCachedThreadPool();
    }
}
