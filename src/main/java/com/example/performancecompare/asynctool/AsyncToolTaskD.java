package com.example.performancecompare.asynctool;

import com.jd.platform.async.callback.ICallback;
import com.jd.platform.async.callback.IWorker;
import com.jd.platform.async.worker.WorkResult;
import com.jd.platform.async.wrapper.WorkerWrapper;
import lombok.SneakyThrows;

import java.util.Map;

/**
 * @program: performance-compare
 * @ClassName AsyncToolTaskA
 * @description:
 * @author: sizegang
 * @create: 2022-12-10
 **/
public class AsyncToolTaskD implements IWorker<String, String>, ICallback<String, String> {

    @Override
    public void begin() {

    }

    @Override
    public void result(boolean b, String s, WorkResult<String> workResult) {

    }

    @SneakyThrows
    @Override
    public String action(String s, Map<String, WorkerWrapper> map) {
        System.out.println("使用" + Thread.currentThread().getName());
        System.out.println("AsyncToolTaskD");
        Thread.sleep(100);
        return null;
    }

    @Override
    public String defaultValue() {
        return null;
    }
}
